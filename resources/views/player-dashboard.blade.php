@extends('layouts.app')

@section('content')
    @if(Auth::User()->supper_user === 1)
        <div class="row justify-content-center" style="padding-top: 20px;background: white">
            <div class="col-md-8 text-center">
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <p style="margin-bottom: 0px;">
                    <h5>Welcome to your Online Portal Review site.</h5><br>
                    After logging in with your User ID and Password you can enter an Account ID to review and/or approve
                    player offers.
                    </p>
                    @if($errors->has('account_id') || $errors->has('account_id'))
                        <span style="color: red">{{$errors->first('account_id') }}</span>
                    @endif
                    <form action="{{ route('view-player-dashboard-by-account-id') }}" method="POST"
                          style="    width: 30%;    margin: 0 auto;">
                        {{ csrf_field() }}
                        <div class="form-group form-group-default" id="emailGroup">
                            <div class="controls">
                                <label><h5>Account ID:</h5></label>
                                <input type="text" name="account_id" id="account_id"
                                       value="{{ old('account_id') }}" placeholder="Account Id"
                                       class="form-control @if($errors->has('account_id')) has-error @endif" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-block login-button">
                            <span class="signingin hidden"><span class="voyager-refresh"></span> LOGIN</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div class="row justify-content-center" style="padding-top: 20px;background: white">
            <div class="columns large-9">
                @if($data->coreSM || $data->cruisePC || $data->weekenderPC ||  $data->corePC)
                    @if($data->corePC)
                        <div class="row">
                            <div class="col-md-12">
                                <!DOCTYPE html>
                                <html lang="en">

                                <head>
                                    <meta charset="UTF-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                    <title>Document</title>
                                </head>

                                <body>
                                <div class="modal-wrap">
                                    <div class="slider-big">
                                        <a href="https://s3.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->corePCResult1}}"
                                           data-lightbox="roadtrip">
                                            <img
                                                src="https://s3.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->corePCResult1}}"
                                                style="width: 75%" alt="">
                                        </a>
                                        <a href="https://s3.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->corePCResult2}}"
                                           data-lightbox="roadtrip">
                                            <img
                                                src="https://s3.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->corePCResult2}}"
                                                style="width: 100%" alt="">
                                        </a>
                                    </div>
                                </div>
                                <script src="/weekender_assets/js/modal_slick.min.js"></script>
                                <script src="/weekender_assets/js/modal_lightbox.min.js"></script>
                                <script src="/weekender_assets/js/modal_main.js"></script>
                                </body>
                                </html>
                            </div>
                        </div>
                    @endif
                    @if($data->coreSM)
                        <div class="row">
                            <div class="col-md-12">
                                <script>

                                </script>
                                <meta name="viewport" content="width = 1050, user-scalable = yes"/>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                                <style>
                                    table {
                                        border-collapse: inherit !important;
                                    }

                                    .lb-details {
                                        display: none;
                                    }

                                    @font-face {
                                        font-family: myFirstFont;
                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                    }

                                    .firstname {
                                        font-family: myFirstFont;
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    @font-face {
                                        font-family: totalfb;
                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                    }

                                    .dollar-amount {
                                        font-family: 'totalfb';
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    /* .dollar-amount62 {
                                      font-family: 'Steelfish', sans-serif;
                                      font-weight: 700;
                                      font-size: 16pt;
                                    } */

                                    #img-magnifier-container {
                                        display: none;
                                        background: rgba(0, 0, 0, 0.8);
                                        border: 5px solid rgb(255, 255, 255);
                                        border-radius: 20px;
                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                        cursor: none;
                                        position: absolute;
                                        pointer-events: none;
                                        width: 400px;
                                        height: 200px;
                                        overflow: hidden;
                                        z-index: 999;
                                    }

                                    .glass {
                                        position: absolute;
                                        background-repeat: no-repeat;
                                        background-size: auto;
                                        cursor: none;
                                        z-index: 1000;
                                    }

                                    #toggle-zoom {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                        background-size: 40px;
                                        display: block;
                                        width: 40px;
                                        display: none;
                                        height: 40px;
                                    }

                                    #printer {
                                        float: right;
                                        display: block;
                                        width: 40px;
                                        height: 40px;
                                        margin-right: 20px;
                                        display: none;
                                    }

                                    #toggle-zoom.toggle-on {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                    }

                                    @media (hover: none) {
                                        .tool-zoom {
                                            display: none;
                                        }

                                        #printer {
                                            display: none;
                                        }
                                    }
                                </style>
                                <div class="flipbook-viewport">
                                    <div class="container">
                                        <div>
                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}"" target="blank"><img
                                                    id="printer"
                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                        </div>
                                        <div class="tool-zoom">
                                            <a id="toggle-zoom" onclick="toggleZoom()"></a>
                                        </div>

                                        <div class="arrows">
                                            <div class="arrow-prev">
                                                <a id="prev"><img class="previous" width="20"
                                                                  src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                  alt=""/></a>
                                            </div>
                                            <center><h4 style="color: red;">Core SM</h4></center>
                                            <div class="flipbook">
                                                <!-- Below is where you change the images for the flipbook -->
                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result1}}"
                                                   data-odd="1"
                                                   id="page-1"
                                                   data-lightbox="big" class="page"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result1}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result2}}"
                                                   data-even="2"
                                                   id="page-2" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result2}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result3}}"
                                                   data-odd="3"
                                                   id="page-3"
                                                   data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result3}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result4}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result4}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result5}}"
                                                   data-odd="5"
                                                   id="page-5"
                                                   data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->result5}}')"></a>

                                                <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->result6}}"
                                                   data-even="6"
                                                   id="page-6" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->result6}}')"></a>
                                            </div>
                                            <div class="arrow-next">
                                                <a id="next"><img class="next" width="20"
                                                                  src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                  alt=""/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Below are the thumbnails to the flipbook -->
                                <div class="flipbook-slider-thumb">
                                    <div class="drag">
                                        <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                        <img onclick="onPageClick(1)" class="thumb-img left-img"
                                             src="{{asset('flipbook_assets/pages/GetGraphic.jpg')}}" alt=""/>

                                        <div class="space">
                                            <img onclick="onPageClick(2)" class="thumb-img"
                                                 src="{{asset('flipbook_assets/pages/GetGraphic_1.jpg')}}" alt=""/>
                                            <img onclick="onPageClick(3)" class="thumb-img"
                                                 src="{{asset('flipbook_assets/pages/GetGraphic_2.jpg')}}" alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClick(4)" class="thumb-img"
                                                 src="{{asset('flipbook_assets/pages/GetGraphic_3.jpg')}}" alt=""/>
                                            <img onclick="onPageClick(5)" class="thumb-img"
                                                 src="{{asset('flipbook_assets/pages/GetGraphic_4.jpg')}}" alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClick(6)" class="thumb-img active"
                                                 src="{{asset('flipbook_assets/pages/GetGraphic_5.jpg')}}" alt=""/>
                                        </div>

                                    <!-- <img id="next-arrow" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                    </div>

                                    <ul class="flipbook-slick-dots" role="tablist">
                                        <li onclick="onPageClick(1)" class="dot">
                                            <a type="button" style="color: #7f7f7f">1</a>
                                        </li>
                                        <li onclick="onPageClick(2)" class="dot">
                                            <a type="button" style="color: #7f7f7f">2</a>
                                        </li>
                                        <li onclick="onPageClick(3)" class="dot">
                                            <a type="button" style="color: #7f7f7f">3</a>
                                        </li>
                                        <li onclick="onPageClick(4)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                        <li onclick="onPageClick(5)" class="dot">
                                            <a type="button" style="color: #7f7f7f">5</a>
                                        </li>
                                        <li onclick="onPageClick(6)" class="dot">
                                            <a type="button" style="color: #7f7f7f">6</a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="img-magnifier-container">
                                    <img id="zoomed-image-container" class="glass" src=""/>
                                </div>
                                <div id="log"></div>
                                <audio id="audio" style="display: none"
                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                <script type="text/javascript">
                                    function scaleFlipBook() {
                                        var imageWidth = 541;
                                        var imageHeight = 850;

                                        var pageHeight = 550;
                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                        $(".flipbook-viewport .container").css({
                                            width: 40 + pageWidth * 2 + 40 + "px",
                                        });

                                        $(".flipbook-viewport .flipbook").css({
                                            width: pageWidth * 2 + "px",
                                            height: pageHeight + "px",
                                        });
                                    }

                                    function doResize() {
                                        $("html").css({
                                            zoom: 1
                                        });
                                        var $viewport = $(".flipbook-viewport");
                                        var viewHeight = $viewport.height();
                                        var viewWidth = $viewport.width();

                                        var $el = $(".flipbook-viewport .container");
                                        var elHeight = $el.outerHeight();
                                        var elWidth = $el.outerWidth();

                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                        if (scale < 1) {
                                            scale *= 0.95;
                                        } else {
                                            scale = 1;
                                        }
                                        $("html").css({
                                            zoom: scale
                                        });
                                    }

                                    function loadApp() {
                                        scaleFlipBook();
                                        var flipbook = $(".flipbook");

                                        // Check if the CSS was already loaded

                                        if (flipbook.width() == 0 || flipbook.height() == 0) {
                                            setTimeout(loadApp, 10);
                                            return;
                                        }

                                        $(".flipbook .double").scissor();

                                        // Create the flipbook

                                        $(".flipbook").turn({
                                            // Elevation

                                            elevation: 50,

                                            // Enable gradients

                                            gradients: true,

                                            // Auto center this flipbook

                                            autoCenter: true,
                                            when: {
                                                turning: function (event, page, view) {
                                                    var audio = document.getElementById("audio");
                                                    audio.play();
                                                },
                                                turned: function (e, page) {
                                                    //console.log('Current view: ', $(this).turn('view'));
                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                    for (var i = 0; i < thumbs.length; i++) {
                                                        var element = thumbs[i];
                                                        if (element.className.indexOf("active") !== -1) {
                                                            $(element).removeClass("active");
                                                        }
                                                    }

                                                    $(
                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                    ).addClass("active");

                                                    var dots = document.getElementsByClassName("dot");
                                                    for (var i = 0; i < dots.length; i++) {
                                                        var dot = dots[i];
                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                            $(dot).removeClass("dot-active");
                                                        }
                                                    }
                                                },
                                            },
                                        });
                                        doResize();
                                    }

                                    $(window).resize(function () {
                                        doResize();
                                    });
                                    $(window).bind("keydown", function (e) {
                                        if (e.keyCode == 37) $(".flipbook").turn("previous");
                                        else if (e.keyCode == 39) $(".flipbook").turn("next");
                                    });
                                    $("#prev").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("previous");
                                    });

                                    $("#next").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("next");
                                    });

                                    $("#prev-arrow").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("previous");
                                    });

                                    $("#next-arrow").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("next");
                                    });

                                    function onPageClick(i) {
                                        $(".flipbook").turn("page", i);
                                    }

                                    // Load the HTML4 version if there's not CSS transform
                                    yepnope({
                                        test: Modernizr.csstransforms,
                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                                        complete: loadApp,
                                    });

                                    zoomToolEnabled = false;

                                    function toggleZoom() {
                                        if (zoomToolEnabled) {
                                            $(".flipbook a").off("mousemove");
                                            $("#toggle-zoom").removeClass("toggle-on");
                                            $("#img-magnifier-container").hide();

                                            zoomToolEnabled = false;
                                        } else {
                                            $(".flipbook a").mousemove(function (event) {
                                                var magnifier = $("#img-magnifier-container");
                                                $("#img-magnifier-container").css(
                                                    "left",
                                                    event.pageX - magnifier.width() / 2
                                                );
                                                $("#img-magnifier-container").css(
                                                    "top",
                                                    event.pageY - magnifier.height() / 2
                                                );
                                                $("#img-magnifier-container").show();
                                                var hoveredImage = $(event.target).css("background-image");
                                                var bg = hoveredImage
                                                    .replace("url(", "")
                                                    .replace(")", "")
                                                    .replace(/\"/gi, "");
                                                // Find relative position of cursor in image.
                                                var targetPage = $(event.target);
                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                var targetTop = 200 / 2; // Height of glass container/2

                                                var zoomedImageContainer = document.getElementById(
                                                    "zoomed-image-container"
                                                );
                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                var imgXPercent =
                                                    (event.pageX - $(event.target).offset().left) /
                                                    targetPage.width();
                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                var imgYPercent =
                                                    (event.pageY - $(event.target).offset().top) /
                                                    targetPage.height();
                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                $("#img-magnifier-container .glass").attr("src", bg);
                                                $("#img-magnifier-container .glass").css(
                                                    "top",
                                                    "" + targetTop + "px"
                                                );
                                                $("#img-magnifier-container .glass").css(
                                                    "left",
                                                    "" + targetLeft + "px"
                                                );
                                            });

                                            $("#toggle-zoom").addClass("toggle-on");
                                            zoomToolEnabled = true;
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col-4 align-self-center">
                            @if($data->weekenderPC)

                                <h4 style="color: red;">Weekender</h4>
                                <div class="col-md-12">
                                    <!DOCTYPE html>
                                    <html lang="en">

                                    <head>
                                        <meta charset="UTF-8">
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                        <title>Document</title>
                                    </head>

                                    <body>
                                    <div class="modal-wrap">
                                        <div class="slider-big">
                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->weekenderPCResult1}}"
                                               data-lightbox="roadtrip">
                                                <img
                                                    src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->weekenderPCResult1}}"
                                                    style="width: 25%" alt="">
                                            </a>
                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/hi_res/{{$data->weekenderPCResult2}}"
                                               data-lightbox="roadtrip">
                                                <img
                                                    src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/{{$data->weekenderPCResult2}}"
                                                    style="width: 100%" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <script src="/weekender_assets/js/modal_slick.min.js"></script>
                                    <script src="/weekender_assets/js/modal_lightbox.min.js"></script>
                                    <script src="/weekender_assets/js/modal_main.js"></script>
                                    </body>
                                    </html>
                                </div>

                            @endif
                        </div>
                        <div class="col-4 align-self-center">
                            @if($data->cruisePC)

                                <h4 style="color: red;">Cruise</h4>
                                <div class="col-md-12 text-center">
                                    <!DOCTYPE html>
                                    <html lang="en">

                                    <head>
                                        <meta charset="UTF-8">
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                                        <title>Document</title>
                                    </head>

                                    <body>
                                    <div class="modal-wrap">
                                        <div class="slider-big">
                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/128462_MAY2021_CRUISE_PC_P01.jpg"
                                               data-lightbox="roadtrip">
                                                <img
                                                    src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/128462_MAY2021_CRUISE_PC_P01.jpg"
                                                    style="width: 25%" alt="">
                                            </a>
                                            <a href="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/128462_MAY2021_CRUISE_PC_P02.jpg"
                                               data-lightbox="roadtrip">
                                                <img
                                                    src="https://s3.us-east-2.amazonaws.com/ballysac.maplewebservices.com/june_2021/lo_res/128462_MAY2021_CRUISE_PC_P02.jpg"
                                                    style="width: 100%" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <script src="/weekender_assets/js/modal_slick.min.js"></script>
                                    <script src="/weekender_assets/js/modal_lightbox.min.js"></script>
                                    <script src="/weekender_assets/js/modal_main.js"></script>
                                    </body>
                                    </html>
                                </div>

                            @endif
                        </div>
                        <div class="col">
                        </div>
                    </div>



                @else
                    We do not have anything for you to see at this time, please check back later.
                    @endif
                    </br>
            </div>
            <div class="columns large-3">
                <a href="/" rel="nofollow"> <!-- Bally&#039;s Atlantic City Hotel and Casino</a> -->
                    @switch($data->BAC_Tier)
                        @case('Red')
                        <img src="{{ asset('assets/images/cards/red.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineRed">Tier Level Red</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:red;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                   <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--</div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:red;"/>
                @break
                @case('Black')
                <img src="{{ asset('assets/images/cards/black.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineBlack">Tier Level Black</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:black;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Red--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:black;"/>
                @break
                @case('Platinum')
                <img src="{{ asset('assets/images/cards/platinum.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlinePlatinum">Tier Level Platinum</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:#e5e4e2;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Black--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:#e5e4e2;"/>
                @break
                @default
                <img src="{{ asset('assets/images/cards/gold.png') }}" alt="bally's card"></a>
                <h4 style="font-weight: 600;color: #232325;">
                    Welcome, {{ $data->first_name }} {{  $data->last_name}}</h4>
                <h5 style="color:red;text-align: center;"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><a
                        href="https://ballysac.maplewebservices.com/admin/logout" style="color:red">Logout</a></h5>
                <div id="TierLevel" class="textlineGold">Tier Level Gold</div>
                {{--                <hr style="height:20px;border:none;color:red;background-color:#FFD700;"/>--}}
                <div id="yourMembershipLevel">
                    <div id="pointsBalanceNextLevel">Tier Score <span
                            style="color:red;">{{  number_format($data->BAC_Tier_Points ?? 0) }}</span>
                    </div>
                    <div id="pointsBalanceNextLevel"> Reward Points <span
                            style="color:red;">${{  $data->BAC_Reward_Points?substr_replace($data->BAC_Reward_Points,'.',strlen($data->BAC_Reward_Points)-2,0):0 }}</span>
                        <br><span style="font-size: 12px;font-style: italic;color: darkgrey;"><i
                                class="fas fa-info-circle"></i> Data updated at {{$data->updated_at}}</span>
                    </div>
                </div>
                {{--            <div id="pointsBalance">--}}
                {{--                <div id="pointsBalanceNextLevelAmt">--}}
                {{--                    <span style="color:red;"> {{  $data->BAC_Points_Next_Tier ?? 0 }}  </span>Tier Credits to advance to--}}
                {{--                    Platinum--}}
                {{--                </div>--}}
                {{--                <div id="cardNextLevel">--}}
                {{--                </div>--}}
                {{--            </div>--}}
                <br>
                <hr style="height:1px;border:none;color:red;background-color:#FFD700;"/>
                @endswitch
            </div>
        </div>
    @endif
@endsection


