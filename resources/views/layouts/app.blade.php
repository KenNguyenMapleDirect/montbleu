<!DOCTYPE html>
<html class="no-js" lang="en-US">

<head>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Google Fonts Below-->
    <link rel="stylesheet" href="https://use.typekit.net/phv1gon.css">

    <!-- Icons & Favicons -->
    <link rel="icon" href="/HeaderFooterAssets/favicons/MountBleuResort-favicon.png">
    <link href="/HeaderFooterAssets/favicons/images-apple-icon-touch.png" rel="apple-touch-icon">
    <!--[if IE]>
    <link rel="shortcut icon" href="https://ballyslaketahoe.com/wp-content/themes/MountBleuResort/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage"
          content="https://ballyslaketahoe.com/wp-content/themes/MountBleuResort/assets/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="https://ballyslaketahoe.com/xmlrpc.php">

    <title>MontBleu Resort Casino &amp; Spa Lake Tahoe</title>
    <meta name="robots" content="max-image-preview:large">
    <script type="text/javascript" src="{{asset('flipbook_assets/js/jquery.min.1.7.js')}}"></script>
    <!-- The SEO Framework by Sybre Waaijer -->
    <meta name="robots" content="max-snippet:-1,max-image-preview:standard,max-video-preview:-1">
    <meta name="description"
          content="Bally&rsquo;s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&rsquo;s AC today.">
    <meta property="og:image"
          content="https://montbleuresort.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="MontBleu Resort Casino &amp; Spa Lake Tahoe">
    <meta property="og:description"
          content="Bally&rsquo;s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&rsquo;s AC today.">
    <meta property="og:url" content="https://ballyslaketahoe.com/">
    <meta property="og:site_name" content="MontBleu Resort Casino &amp; Spa Lake Tahoe">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="MontBleu Resort Casino &amp; Spa Lake Tahoe">
    <meta name="twitter:description"
          content="Bally&rsquo;s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&rsquo;s AC today.">
    <meta name="twitter:image"
          content="https://montbleuresort.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg">
    <link rel="canonical" href="https://ballyslaketahoe.com/">
    <script type="application/ld+json">
        {"@context":"https://schema.org","@type":"WebSite","url":"https://ballyslaketahoe.com/","name":"MontBleu Resort Casino &amp; Spa Lake Tahoe","potentialAction":{"@type":"SearchAction","target":"https://ballyslaketahoe.com/search/{search_term_string}/","query-input":"required name=search_term_string"}}


    </script>
    <script type="application/ld+json">
        {"@context":"https://schema.org","@type":"Organization","url":"https://ballyslaketahoe.com/","name":"MontBleu Resort Casino &amp; Spa Lake Tahoe","logo":"https://montbleuresort.wpengine.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg"}


    </script>
    <!-- / The SEO Framework by Sybre Waaijer | 0.18ms meta | 0.84ms boot -->

    <link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
    <link rel="dns-prefetch" href="//s.w.org">
    <link rel="alternate" type="application/rss+xml" title="MontBleu Resort Casino &amp; Spa Lake Tahoe &raquo; Feed"
          href="https://ballyslaketahoe.com/feed/">
    <link rel="alternate" type="application/rss+xml"
          title="MontBleu Resort Casino &amp; Spa Lake Tahoe &raquo; Comments Feed"
          href="https://ballyslaketahoe.com/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/ballyslaketahoe.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.7.1"}
        };
        !function (e, a, t) {
            var n, r, o, i = a.createElement("canvas"), p = i.getContext && i.getContext("2d");

            function s(e, t) {
                var a = String.fromCharCode;
                p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0);
                e = i.toDataURL();
                return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
            }

            function c(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }

            for (o = Array("flag", "emoji"), t.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, r = 0; r < o.length; r++) t.supports[o[r]] = function (e) {
                if (!p || !p.fillText) return !1;
                switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
                    case"flag":
                        return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
                    case"emoji":
                        return !s([55357, 56424, 8205, 55356, 57212], [55357, 56424, 8203, 55356, 57212])
                }
                return !1
            }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () {
                t.DOMReady = !0
            }, t.supports.everything || (n = function () {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () {
                "complete" === a.readyState && t.readyCallback()
            })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="/HeaderFooterAssets/css/block-library-style.min.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="normalize-css-css" href="/HeaderFooterAssets/css/css-normalize.min.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="foundation-css-css" href="/HeaderFooterAssets/css/css-foundation.min.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="site-css-css" href="/HeaderFooterAssets/css/css-style.css" type="text/css" media="all">

    <link rel='stylesheet' id='font-awesome-css'
          href='{{ asset('assets/font-awesome/4.3.0/css/font-awesome.min40df.css?ver=5.6') }}' type='text/css'
          media='all'/>
    <link rel="https://api.w.org/" href="https://ballyslaketahoe.com/wp-json/">
    <link rel="alternate" type="application/json" href="https://ballyslaketahoe.com/wp-json/wp/v2/pages/19">
    <link rel="alternate" type="application/json+oembed"
          href="https://ballyslaketahoe.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballyslaketahoe.com%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://ballyslaketahoe.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballyslaketahoe.com%2F&amp;format=xml">
    <style type="text/css" id="wp-custom-css">

        .top-bar-section ul ul ul {
            width: 150% !important;
        }

        .page-title {
            display: none;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }        </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NQJMR2BCMV"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-NQJMR2BCMV');
    </script>
    <link href="/weekender_assets/css/modal_slick.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_slick-theme.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_lightbox.min.css" rel="stylesheet">
    <link href="/weekender_assets/css/modal_style.css" rel="stylesheet">
</head>

<body class="home page-template-default page page-id-19">
<aside role="alert" class="alert-banner">
    <div class="pagewidth">
        <strong>Health &amp; Safety Information <a href="health-and-safety.html" target="_self">Click for Details
                &rsaquo;</a></strong>

    </div>
</aside>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
        <div id="container">
            <header class="header" role="banner">
                <div id="inner-header" class="row clearfix">
                    <!-- This navs will be applied to the topbar, above all content
                         To see additional nav styles, visit the /parts directory -->
                    <div class="show-for-medium-up contain-to-grid">
                        <nav class="top-bar" data-topbar>
                            <ul class="title-area">
                                <!-- Title Area -->
                                <li class="name">
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href="ballyslaketahoe.html" rel="nofollow">
                                        <!-- MontBleu Resort Casino &amp; Spa Lake Tahoe</a> -->
                                        <img src="/HeaderFooterAssets/images/05-Montbleu_Logo_Blue-386x165-1.png"
                                             alt="MontBleu Resort logo"
                                             width="386"></a>
                                </li>
                            </ul>
                            <section class="top-bar-section right">
                                <ul id="menu-main-navigation" class="top-bar-menu right">
                                    <li class="divider">
                                    <li id="menu-item-5031"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5031">
                                        <a href="rooms-suites.html">Rooms &amp; Suites</a></li>
                                    <li class="divider">
                                    <li id="menu-item-5037"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5037">
                                        <a href="gaming.html">Gaming</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5500"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5500">
                                                <a href="player-portal.html">Player Portal</a></li>
                                            <li id="menu-item-5049"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5049">
                                                <a href="promotions-and-tournaments.html">Promotions and Tournaments</a>
                                            </li>
                                            <li id="menu-item-5048"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5048">
                                                <a href="services.html">Services</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-5326"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5326">
                                        <a href="spa.html">Spa</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5332"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5332">
                                                <a href="massage.html">Massage</a></li>
                                            <li id="menu-item-5328"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5328">
                                                <a href="body-treatments.html">Body Treatments</a></li>
                                            <li id="menu-item-5329"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5329">
                                                <a href="facial.html">Facial</a></li>
                                            <li id="menu-item-5330"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5330">
                                                <a href="hair.html">Hair</a></li>
                                            <li id="menu-item-5327"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5327">
                                                <a href="waxing-tinting.html">Waxing &amp; Tinting</a></li>
                                            <li id="menu-item-5333"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5333">
                                                <a href="nails.html">Nails</a></li>
                                            <li id="menu-item-5331"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5331">
                                                <a href="make-up-services.html">Make-Up Services</a></li>
                                            <li id="menu-item-5525"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5525">
                                                <a href="pool-and-fitness-center.html">Pool and Fitness Center</a></li>
                                            <li id="menu-item-5526"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5526">
                                                <a href="fitness-membership-fees.html">Fitness Membership Fees</a></li>
                                            <li id="menu-item-5527"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5527">
                                                <a href="bridal-packages.html">Bridal Packages</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-4845"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4845">
                                        <a href="restaurants.html">Restaurants</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5385"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5385">
                                                <a href="ciera-steak-chophouse.html">Ciera Steak + Chophouse</a></li>
                                            <li id="menu-item-5384"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5384">
                                                <a href="cafe-del-soul.html">Caf&eacute; Del Soul</a></li>
                                            <li id="menu-item-5383"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5383">
                                                <a href="fortune.html">Fortune</a></li>
                                            <li id="menu-item-5382"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5382">
                                                <a href="montbleu-cafe.html">MontBleu Cafe</a></li>
                                            <li id="menu-item-5381"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5381">
                                                <a href="hq-center-bar.html">HQ Center Bar</a></li>
                                            <li id="menu-item-5380"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5380">
                                                <a href="craft-beer-bar.html">Craft Beer Bar</a></li>
                                            <li id="menu-item-5379"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5379">
                                                <a href="the-zone-bar.html">The Zone Bar</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-5387"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5387">
                                        <a href="weddings.html">Weddings</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5429"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5429">
                                                <a href="convention-space.html">Convention Space</a></li>
                                            <li id="menu-item-5428"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5428">
                                                <a href="opal-wedding-reception.html">Opal Wedding Reception</a></li>
                                            <li id="menu-item-5427"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5427">
                                                <a href="wedding-cake-referrals.html">Wedding Cake Referrals</a></li>
                                            <li id="menu-item-5426"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5426">
                                                <a href="lake-tahoe-wedding-receptions.html">Lake Tahoe Wedding
                                                    Receptions</a></li>
                                            <li id="menu-item-5425"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5425">
                                                <a href="photography-and-video.html">Photography and Video</a></li>
                                            <li id="menu-item-5424"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5424">
                                                <a href="marriage-license-information.html">Marriage License
                                                    Information</a></li>
                                            <li id="menu-item-5423"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5423">
                                                <a href="ala-carte-pricing.html">Ala Carte Pricing</a></li>
                                            <li id="menu-item-5422"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5422">
                                                <a href="chapel-collections.html">Chapel Collections</a></li>
                                            <li id="menu-item-5421"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5421">
                                                <a href="bridal-packages.html">Bridal Packages</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-5039"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5039">
                                        <a href="entertainment.html">Entertainment</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5540"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5540">
                                                <a href="shows.html">Shows</a></li>
                                            <li id="menu-item-5539"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5539">
                                                <a href="opal-ultra-lounge.html">Nightlife</a></li>
                                            <li id="menu-item-5544"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-dropdown menu-item-5544">
                                                <a href="ballyslaketahoe.html">Shops</a>
                                                <ul class="sub-menu dropdown">
                                                    <li id="menu-item-5536"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5536">
                                                        <a href="bleu-market.html">Bleu Market</a></li>
                                                    <li id="menu-item-5535"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5535">
                                                        <a href="bluezone-sports.html">BlueZone Sports</a></li>
                                                    <li id="menu-item-5537"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5537">
                                                        <a href="romantic-adventures.html">Romantic Adventures</a></li>
                                                    <li id="menu-item-5538"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5538">
                                                        <a href="lake-tahoe-cigar-co.html">Lake Tahoe Cigar Co</a></li>
                                                </ul>
                                            </li>
                                            <li id="menu-item-5543"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5543">
                                                <a href="puzzle-room.html">Puzzle Room</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-5035"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-5035">
                                        <a href="meetings.html">Meetings</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-5520"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5520">
                                                <a href="plan-a-meeting.html">Plan A Meeting</a></li>
                                            <li id="menu-item-5483"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5483">
                                                <a href="contact-information.html">Contact Information</a></li>
                                            <li id="menu-item-5482"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5482">
                                                <a href="audio-visual.html">Audio Visual</a></li>
                                            <li id="menu-item-5481"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5481">
                                                <a href="catering.html">Catering</a></li>
                                            <li id="menu-item-5480"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5480">
                                                <a href="montbleu-theater.html">MontBleu Theater</a></li>
                                            <li id="menu-item-5479"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5479">
                                                <a href="convention-center.html">Convention Center</a></li>
                                            <li id="menu-item-5478"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5478">
                                                <a href="aspen-boardroom.html">Aspen Boardroom</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider">
                                    <li id="menu-item-5026"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5026">
                                        <a href="offers.html">Offers</a></li>
                                    @if ((Auth::User()))
                                        @if (Auth::user()->role_id === 2)
                                            <li class="divider">
                                            <li id="menu-item-5026"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5026"
                                                onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011">
                                                <a href="{{ route('voyager.logout') }}">Logout</a></li>
                                            <form id="logout-form" action="{{ route('voyager.logout') }}" method="POST"
                                                  style="display: none;">
                                                @csrf
                                            </form>
                                        @endif
                                    @endif
                                </ul>
                            </section>

                            <div id="book-a-room">
                                <button><a href="https://Ballyslaketahoe.reztrip.com">Book a Room</a></button>
                            </div>

                        </nav>
                    </div>

                    <div class="sticky fixed show-for-small">
                        <nav class="tab-bar">
                            <section class="middle tab-bar-section">
                                <a href="ballyslaketahoe.html" rel="nofollow"><h1 class="title">MontBleu Resort</h1></a>
                            </section>
                            <section class="left-small">
                                <a href="#" class="left-off-canvas-toggle menu-icon"><span><i
                                            class="fa fab fa-bars"></i></span></a>
                            </section>
                        </nav>
                    </div>

                    <aside class="left-off-canvas-menu show-for-small-only">
                        <ul class="off-canvas-list">
                            <!-- <li><label>Navigation</label></li> -->
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<a href="ballyslaketahoe.html"><span
                                    class="home-link">Home</span></a>

                            <ul id="menu-main-navigation-1" class="off-canvas-list">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5031"><a
                                        href="rooms-suites.html">Rooms &amp; Suites</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5037">
                                    <a href="gaming.html">Gaming</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5500">
                                            <a href="player-portal.html">Player Portal</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5049">
                                            <a href="promotions-and-tournaments.html">Promotions and Tournaments</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5048">
                                            <a href="services.html">Services</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5326">
                                    <a href="spa.html">Spa</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5332">
                                            <a href="massage.html">Massage</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5328">
                                            <a href="body-treatments.html">Body Treatments</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5329">
                                            <a href="facial.html">Facial</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5330">
                                            <a href="hair.html">Hair</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5327">
                                            <a href="waxing-tinting.html">Waxing &amp; Tinting</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5333">
                                            <a href="nails.html">Nails</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5331">
                                            <a href="make-up-services.html">Make-Up Services</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5525">
                                            <a href="pool-and-fitness-center.html">Pool and Fitness Center</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5526">
                                            <a href="fitness-membership-fees.html">Fitness Membership Fees</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5527">
                                            <a href="bridal-packages.html">Bridal Packages</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4845">
                                    <a href="restaurants.html">Restaurants</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5385">
                                            <a href="ciera-steak-chophouse.html">Ciera Steak + Chophouse</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5384">
                                            <a href="cafe-del-soul.html">Caf&eacute; Del Soul</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5383">
                                            <a href="fortune.html">Fortune</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5382">
                                            <a href="montbleu-cafe.html">MontBleu Cafe</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5381">
                                            <a href="hq-center-bar.html">HQ Center Bar</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5380">
                                            <a href="craft-beer-bar.html">Craft Beer Bar</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5379">
                                            <a href="the-zone-bar.html">The Zone Bar</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5387">
                                    <a href="weddings.html">Weddings</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5429">
                                            <a href="convention-space.html">Convention Space</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5428">
                                            <a href="opal-wedding-reception.html">Opal Wedding Reception</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5427">
                                            <a href="wedding-cake-referrals.html">Wedding Cake Referrals</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5426">
                                            <a href="lake-tahoe-wedding-receptions.html">Lake Tahoe Wedding
                                                Receptions</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5425">
                                            <a href="photography-and-video.html">Photography and Video</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5424">
                                            <a href="marriage-license-information.html">Marriage License Information</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5423">
                                            <a href="ala-carte-pricing.html">Ala Carte Pricing</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5422">
                                            <a href="chapel-collections.html">Chapel Collections</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5421">
                                            <a href="bridal-packages.html">Bridal Packages</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5039">
                                    <a href="entertainment.html">Entertainment</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5540">
                                            <a href="shows.html">Shows</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5539">
                                            <a href="opal-ultra-lounge.html">Nightlife</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-5544">
                                            <a href="ballyslaketahoe.html">Shops</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5536">
                                                    <a href="bleu-market.html">Bleu Market</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5535">
                                                    <a href="bluezone-sports.html">BlueZone Sports</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5537">
                                                    <a href="romantic-adventures.html">Romantic Adventures</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5538">
                                                    <a href="lake-tahoe-cigar-co.html">Lake Tahoe Cigar Co</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5543">
                                            <a href="puzzle-room.html">Puzzle Room</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5035">
                                    <a href="meetings.html">Meetings</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5520">
                                            <a href="plan-a-meeting.html">Plan A Meeting</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5483">
                                            <a href="contact-information.html">Contact Information</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5482">
                                            <a href="audio-visual.html">Audio Visual</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5481">
                                            <a href="catering.html">Catering</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5480">
                                            <a href="montbleu-theater.html">MontBleu Theater</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5479">
                                            <a href="convention-center.html">Convention Center</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5478">
                                            <a href="aspen-boardroom.html">Aspen Boardroom</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5026"><a
                                        href="offers.html">Offers</a></li>
                            </ul>
                        </ul>
                    </aside>

                    <a class="exit-off-canvas" href="ballyslaketahoe.html"></a>
                </div>  <!-- end #inner-header -->
            </header> <!-- end .header -->
            <div class="loader"></div>
            <div id="content" class="content">
                @yield('content')
            </div>
            <footer class="footer" role="contentinfo">
                <div id="inner-footer" class="row clearfix fullwidth">
                    <div class="large-12 medium-12 columns">
                        <div id="execphp-17" class="widget-odd widget-first widget-1 widget widget_execphp">
                            <div class="execphpwidget"><p>An inherent risk of exposure to COVID-19 exists in any public
                                    place where
                                    people are present. COVID-19 is an extremely contagious disease that can lead to
                                    severe illness
                                    and even death. According to the <a
                                        href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers
                                        for Disease Control and Prevention,</a> senior citizens and guests with
                                    underlying medical
                                    conditions are especially vulnerable. By visiting MontBleu Resort Casino &amp; Spa
                                    Lake Tahoe
                                    you voluntarily assume all risks related to exposure to COVID-19.</p>

                                <a href="https://twitter.com/montbleuresort"><span class="fa fa-twitter-square"
                                                                                   title="Twitter"
                                                                                   target="_blank"></span><span
                                        class="screen-reader-text">Like Us on Facebook</span></a>

                                <a href="https://www.facebook.com/MontBleuResort"><span class="fa fa-facebook-square"
                                                                                        title="Twitter"
                                                                                        target="_blank"></span><span
                                        class="screen-reader-text">Follow Us on Twitter</span></a>

                                <a href="https://www.instagram.com/montbleutahoe/"><span class="fa fa-instagram"
                                                                                         title="instagram"
                                                                                         target="_blank"></span><span
                                        class="screen-reader-text">Follow Us on Instagram</span></a>


                                <style>a span.screen-reader-text {
                                        clip: rect(1px, 1px, 1px, 1px);
                                        position: absolute !important;
                                        height: 1px;
                                        width: 1px;
                                        overflow: hidden;
                                        color: #fff;
                                        font-size: 20px;
                                    }</style>
                            </div>
                        </div>
                        <div id="nav_menu-4"
                             class="widget-even widget-2 favorite-links-footer-widget widget widget_nav_menu"><h2
                                class="widgettitle">Additional Links</h2>
                            <div class="menu-footer-menu-container">
                                <ul id="menu-footer-menu" class="menu">
                                    <li id="menu-item-5460"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5460">
                                        <a
                                            href="contact-us.html">Contact Us</a></li>
                                    <li id="menu-item-5225"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-5225">
                                        <a
                                            href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=F0B828C9EF7461330F62726E66C8CEC0">Employment
                                            Opportunities</a></li>
                                    <li id="menu-item-5240"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5240">
                                        <a
                                            href="accessibility-statement.html">Accessibility Statement</a></li>
                                    <li id="menu-item-5238"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5238">
                                        <a
                                            href="awards-and-accolades.html">Awards and Accolades</a></li>
                                    <li id="menu-item-5239"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5239">
                                        <a
                                            href="lost-and-found.html">Lost and Found</a></li>
                                    <li id="menu-item-5456"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5456">
                                        <a
                                            href="property-map.html">Property Map</a></li>
                                    <li id="menu-item-5237"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5237">
                                        <a
                                            href="about.html">About</a></li>
                                    <li id="menu-item-5458"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5458">
                                        <a
                                            href="bleu-goes-green.html">Bleu Goes Green</a></li>
                                    <li id="menu-item-5236"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5236">
                                        <a
                                            href="frequently-asked-questions.html">Frequently Asked Questions</a></li>
                                    <li id="menu-item-5459"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5459">
                                        <a
                                            href="responsible-gaming.html">Responsible Gaming</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="execphp-18"
                             class="widget-odd widget-last widget-3 join-our-mailing-list-widget widget widget_execphp">
                            <h2
                                class="widgettitle">Bally&rsquo;s Corp. Properties</h2>
                            <div class="execphpwidget"><a href="https://www.twinriver.com/" target="_blank"
                                                          rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-01-01.png"
                                        width="30%"
                                        alt="Twin River Casino Hotel"> </a>
                                <a href="https://www.hrhcbiloxi.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-02-01.png"
                                        width="30%"
                                        alt="Hard Rock Hotel &amp; Casino Biloxi"> </a>
                                <a href="https://www.goldenmardigras.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-03-01.png"
                                        width="30%"
                                        alt="Mardi Gras Casinos"> </a>
                                <br>
                                <a href="https://www.eldoradoshreveport.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-10-01.png"
                                        width="30%"
                                        alt="ElDorado Shreveport"> </a>
                                <a href="https://www.twinrivertiverton.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-05-01.png%22"
                                        width="30%"
                                        alt="Tiverton Casino Hotel"> </a>
                                <a href="https://www.doverdowns.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-06-01.png"
                                        width="30%"
                                        alt="Dover Downs Hotel &amp; Casino"> </a>
                                <br>
                                <a href="http://www.mihiracing.com/mhre/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-07-01.png"
                                        width="30%" alt="Arapahoe Park">
                                </a>
                                <a href="https://casinokc.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-08-01.png"
                                        width="30%" alt="Casino KC"> </a>
                                <a href="https://casinovb.com/" target="_blank" rel="noopener"><img
                                        src="/HeaderFooterAssets/images/03-Bally_Individual-AllProperties-09-01.png"
                                        width="30%" alt="Casino Vicksburg">
                                </a>
                                <br></div>
                        </div>
                        <div style="clear: both"></div>
                        <div id="execphp-19"
                             class="widget-odd widget-last widget-first widget-1 post-footer widget_execphp center">
                            <div class="execphpwidget"><p>MontBleu Resort Casino &amp; Spa Lake Tahoe<br>
                                    55 Highway 50<br>
                                    Stateline, Nevada 89449</p>

                                <p>Toll-Free: <a href="tel:800-235-8259">1-800-BE-LUCKY</a></p>

                                <p>Copyright &copy;2021 MontBleu Casino &amp; Spa Lake Tahoe&reg;. All rights reserved.
                                    | <a
                                        href="privacy-policy.html">Privacy Policy</a> | <a href="cookie-policy.html">Cookie
                                        Policy</a></p></div>
                        </div>
                    </div>
                </div> <!-- end #inner-footer -->
            </footer> <!-- end .footer -->
        </div> <!-- end #container -->
    </div> <!-- end .inner-wrap -->
</div> <!-- end .off-canvas-wrap -->
<link rel="stylesheet" id="soliloquy-lite-style-css" href="/HeaderFooterAssets/css/css-soliloquy.css" type="text/css"
      media="all">
<script type="text/javascript" src="/HeaderFooterAssets/js/vendor-jquery.js" id="jquery-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/vendor-modernizr.js" id="modernizr-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/js-foundation.min.js" id="foundation-js-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/js-scripts.js" id="site-js-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/js-jquery.bxslider.js" id="bx-slider-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/js-isotope.pkgd.min.js" id="isotope-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/js-wp-embed.min.js" id="wp-embed-js"></script>
<script type="text/javascript" src="/HeaderFooterAssets/js/min-soliloquy-min.js" id="soliloquy-lite-script-js"></script>
<script type="text/javascript">
    if (window.jQuery) {
        jQuery(document).ready(function ($) {
            var gforms = '.gform_wrapper form';
            $(document).on('submit', gforms, function () {
                $('<input>').attr('type', 'hidden')
                    .attr('name', 'gf_zero_spam_key')
                    .attr('value', 'E6RXemcGzs3LlRRGgP62ubR6pZKfuybT2djV64KP5BrbXhPsQ3MctDFLxj5wcz35')
                    .appendTo(gforms);
                return true;
            });
        });
    }
</script>
<script type="text/javascript">
    if (typeof soliloquy_slider === 'undefined' || false === soliloquy_slider) {
        soliloquy_slider = {};
    }
    jQuery('#soliloquy-container-20').css('height', Math.round(jQuery('#soliloquy-container-20').width() / (1185 / 545)));
    jQuery(window).load(function () {
        var $ = jQuery;
        var soliloquy_container_20 = $('#soliloquy-container-20'), soliloquy_20 = $('#soliloquy-20');
        soliloquy_slider['20'] = soliloquy_20.soliloquy({
            slideSelector: '.soliloquy-item',
            speed: 650,
            pause: 6000,
            auto: 1,
            useCSS: 0,
            keyboard: true,
            adaptiveHeight: 1,
            adaptiveHeightSpeed: 400,
            infiniteLoop: 1,
            mode: 'fade',
            pager: 1,
            controls: 1,
            nextText: '',
            prevText: '',
            startText: '',
            stopText: '',
            onSliderLoad: function (currentIndex) {
                soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden', 'true');
                soliloquy_container_20.css({'height': 'auto', 'background-image': 'none'});
                if (soliloquy_container_20.find('.soliloquy-slider li').length > 1) {
                    soliloquy_container_20.find('.soliloquy-controls').fadeTo(300, 1);
                }
                soliloquy_20.find('.soliloquy-item:not(.soliloquy-clone):eq(' + currentIndex + ')').addClass('soliloquy-active-slide').attr('aria-hidden', 'false');
                soliloquy_container_20.find('.soliloquy-clone').find('*').removeAttr('id');
                soliloquy_container_20.find('.soliloquy-controls-direction').attr('aria-label', 'carousel buttons').attr('aria-controls', 'soliloquy-container-20');
                soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-prev').attr('aria-label', 'previous');
                soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-next').attr('aria-label', 'next');
            },
            onSlideBefore: function (element, oldIndex, newIndex) {
                soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden', 'true');
                $(element).addClass('soliloquy-active-slide').attr('aria-hidden', 'false');
            },
            onSlideAfter: function (element, oldIndex, newIndex) {
            },
        });
    });            </script>
</body>
<script>
    $(window).load(function () {
        $(".loader").fadeOut("slow");
    });
</script>
</html><!-- end page --> <!-- end .footer -->



