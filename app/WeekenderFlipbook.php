<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WeekenderFlipbook extends Model
{
    protected $table = 'WeekenderFlipbooks';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'Flipbook_Account';
}
