<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class May2021 extends Model
{
    protected $table = 'May2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

